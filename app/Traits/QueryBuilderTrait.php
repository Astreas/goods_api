<?php

namespace App\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Trait QueryBuilderTrait
 *
 * Use same fields for models connection, remove 'queryBuilderTrait' from variable names
 *
 * @package App\Traits
 */
trait QueryBuilderTrait
{
    /** @var string[] $queryBuilderTraitAllowedFilters fields allowed for filters */
    private array $queryBuilderTraitAllowedFilters = [];

    /** @var string[] $queryBuilderTraitAllowedFields fields allowed for select */
    private array $queryBuilderTraitAllowedFields = [];

    /** @var string[] $queryBuilderTraitAllowedAppends accessors allowed for select */
    private array $queryBuilderTraitAllowedAppends = [];

    /**
     * @var string[] $queryBuilderTraitAllowedRelations allowed relations
     */
    private array $queryBuilderTraitAllowedRelations = [];

    /** @var string[] $queryBuilderTraitAllowedSorts fields allowed for sort */
    private array $queryBuilderTraitAllowedSorts = [];

    /**
     * Prepare builder instance
     *
     * @return QueryBuilder
     */
    public static function makeBuilder(): QueryBuilder
    {
        $builder = QueryBuilder::for(self::class)->defaultSort('-created_at');
        $object = (new self);
        $search = request()->query('search');
        if (!empty($search) && $builder->hasNamedScope('search')) {
            $builder->search($search);
        }
        if ($object->queryBuilderTraitAllowedFields) {
            $builder->allowedFields($object->queryBuilderTraitAllowedFields);
        }
        if ($object->queryBuilderTraitAllowedAppends) {
            $builder->allowedAppends($object->queryBuilderTraitAllowedAppends);
        }
        if ($object->queryBuilderTraitAllowedFilters) {
            $builder->allowedFilters($object->queryBuilderTraitAllowedFilters);
        }
        if ($object->queryBuilderTraitAllowedSorts) {
            $builder->allowedSorts($object->queryBuilderTraitAllowedSorts);
        }
        if ($object->queryBuilderTraitAllowedRelations) {
            $builder->allowedIncludes($object->queryBuilderTraitAllowedRelations);
        }

        return $builder;
    }

    /**
     * Initial setting up - use fillable/hidden attributes as default
     */
    protected function initializeQueryBuilderTrait(): void
    {
        $availableFields = array_diff($this->getFillable(), $this->getHidden());
        $this->queryBuilderTraitAllowedFields = $this->allowedFields ?? $availableFields;
        $this->queryBuilderTraitAllowedAppends = $this->allowedAppends ?? [];
        if (method_exists($this, 'getAllowedFilters')) {
            $this->queryBuilderTraitAllowedFilters = $this->getAllowedFilters();
        } else {
            $this->queryBuilderTraitAllowedFilters = $this->allowedFilters ?? $availableFields;
        }

        if (method_exists($this, 'getAllowedSorts')) {
            $this->queryBuilderTraitAllowedSorts = $this->getAllowedSorts();
        } else {
            $this->queryBuilderTraitAllowedSorts = $this->allowedSorts ?? $availableFields;
        }
        $this->queryBuilderTraitAllowedRelations = $this->allowedRelations ?? [];
    }

    /**
     * Digit filter setter
     *
     * @param $column
     *
     * @return AllowedFilter
     */
    public function setDigitFilter($column): AllowedFilter
    {
        return AllowedFilter::callback($column, static function (Builder $builder, $value) use ($column) {
            return $builder->betweenFilter($column, $value);
        });
    }

    /**
     * Scope for between filter
     *
     * @param Builder $builder
     * @param string $column
     * @param string $value_raw
     *
     * @return Builder
     */
    public function scopeBetweenFilter(Builder $builder, string $column, string $value_raw): Builder
    {
        if (str_contains($value_raw, ':')) {
            $value = explode(':', $value_raw);

            return $builder->whereBetween($column, $value);
        }

        $operator = '=';
        if (str_contains($value_raw, '=')) {
            $operator = substr($value_raw, 0, 2);
        } elseif (str_contains($value_raw, '>') || str_contains($value_raw, '<')) {
            $operator = substr($value_raw, 0, 1);
        }

        $value = preg_replace('/[^\d-]/', '', $value_raw);

        return $builder->where($column, $operator, $value);
    }

    /**
     * Set # of records per page from request or from config file
     *
     * @param Builder $builder
     * @return LengthAwarePaginator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function scopeApiPaginate(Builder $builder): LengthAwarePaginator
    {
        $per_page_default = config('query-builder.per_page_default');
        $per_page = request()->get('per_page', $per_page_default);

        return $builder->paginate($per_page);
    }
}
