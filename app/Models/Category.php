<?php

namespace App\Models;

use App\Traits\QueryBuilderTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    use QueryBuilderTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @var array|string[]
     */
    protected array $allowedRelations = [
    ];

    /**
     * Search scope for query builder
     *
     * @param Builder $builder
     * @param string $value
     * @return Builder
     */
    public function scopeSearch(Builder $builder, string $value): Builder
    {
        return $builder->where(function ($query) use ($value) {
            $search_query = '%' . $value . '%';
            return $query->where('title', 'ILIKE', $search_query);
        });
    }

    /**
     * Allowed filters (query builder)
     *
     * @return array
     */
    public function getAllowedFilters(): array
    {
        return [
            'title',
        ];
    }

    /**
     * Allowed sort (query builder)
     *
     * @return array
     */
    public function getAllowedSorts(): array
    {
        return [
            'title'
        ];
    }

    /**
     * Products relation
     *
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'category_product',
            'category_id',
            'product_id',
            'id'
        );
    }
}
