<?php

namespace App\Models;

use App\Traits\QueryBuilderTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\QueryBuilder\AllowedFilter;

class Product extends Model
{
    use QueryBuilderTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'price',
        'is_published',
    ];

    /**
     * List of allowed relations (query builder)
     *
     * @var array|string[]
     */
    protected array $allowedRelations = [
        'categories'
    ];

    /**
     * Search scope for query builder
     *
     * @param Builder $builder
     * @param string $value
     * @return Builder
     */
    public function scopeSearch(Builder $builder, string $value): Builder
    {
        return $builder->where(function ($query) use ($value) {
            $search_query = '%' . $value . '%';
            return $query->where('title', 'ILIKE', $search_query);
        });
    }

    /**
     * Allowed filters (query builder)
     *
     * @return array
     */
    public function getAllowedFilters(): array
    {
        return [
            'title',
            $this->setDigitFilter('price'),
            'is_published',
            AllowedFilter::scope('categoryTitle'),
            AllowedFilter::trashed()
        ];
    }

    /**
     * Category title scope for query builder searching
     *
     * @param Builder $query
     * @param string $categoryTitle
     */
    public function scopeCategoryTitle(Builder $query, string $categoryTitle)
    {
        $query->whereHas('categories', function (Builder $subQuery) use ($categoryTitle) {
            $subQuery->where('title', 'like', '%' . $categoryTitle . '%');
        });
    }

    /**
     * Allowed sort (query builder)
     * @return array
     */
    public function getAllowedSorts(): array
    {
        return [
            'title',
            'price',
            'is_published'
        ];
    }

    /**
     * Categories relation
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Category::class,
            'category_product',
            'product_id',
            'category_id',
            'id'
        );
    }
}
