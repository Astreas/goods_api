<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use function abort;
use function response;

/**
 * Category Controller
 */
class CategoryController extends Controller
{

    /**
     * Categories list
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $categories = Category::makeBuilder()->apiPaginate();

        return CategoryResource::collection($categories);
    }


    /**
     * Create category
     *
     * @param CategoryRequest $request
     * @return CategoryResource
     */
    public function store(CategoryRequest $request): CategoryResource
    {
        $category = Category::create($request->validated());

        return (new CategoryResource($category));
    }


    /**
     * Update category
     *
     * @param Category $category
     * @param CategoryRequest $request
     * @return CategoryResource
     */
    public function update(Category $category, CategoryRequest $request): CategoryResource
    {
        $category = $category->update($request->validated());

        return (new CategoryResource($category));
    }


    /**
     * Delete category
     *
     * @param Category $category
     * @return Response
     */
    public function destroy(Category $category): Response
    {

        $hasProducts = $category->products()->isNotEmpty();

        if ($hasProducts) {
            abort(422, trans('exceptions.location_has_admin'));
        }

        $category->delete();

        return response()->noContent();
    }
}
