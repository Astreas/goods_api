<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use function response;

/**
 * Product Controller
 */
class ProductController extends Controller
{

    /**
     * Product service
     *
     * @var ProductService
     */
    protected ProductService $service;

    /**
     * Controller constructor
     *
     * @param ProductService $service
     */
    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    /**
     * Products list
     *
     * @return JsonResource
     */
    public function index(): JsonResource
    {
        $products = Product::makeBuilder()->apiPaginate();

        return ProductResource::collection($products);
    }

    /**
     * Create product
     *
     * @param ProductRequest $request
     * @return ProductResource
     */
    public function store(ProductRequest $request)
    {
        $product = $this->service->store($request->validated());

        return (new ProductResource($product));
    }

    /**
     * Update product
     *
     * @param Product $product
     * @param ProductRequest $request
     * @return JsonResource
     */
    public function update(Product $product, ProductRequest $request): JsonResource
    {
        $product = $this->service->update($product, $request->validated());

        return (new ProductResource($product));
    }


    /**
     * Delete product (soft-delete)
     *
     * @param Product $product
     * @return Response
     */
    public function destroy(Product $product): Response
    {
        $product->delete();

        return response('', 204);
    }

    /**
     * Restore deleted product
     *
     * @param $id
     * @return Response
     */
    public function restore($id): Response
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();

        return response('', 204);
    }
}
