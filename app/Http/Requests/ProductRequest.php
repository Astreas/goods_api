<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ruleTitleUnique = Rule::unique((new Product())->getTable(), 'title');

        if ($product = $this->route('product')) {
            $ruleTitleUnique->ignore($product, 'id');
        }

        return [
            'title' => ['required', 'string', $ruleTitleUnique],
            'price' => ['required', 'numeric', 'min:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'categories' => ['required', 'array', 'min:2', 'max:10'],
            'categories.*' => ['integer', 'exists:categories,id'],
            'is_published' => ['sometimes', 'boolean']
        ];
    }
}
