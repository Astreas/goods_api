<?php

namespace App\Http\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ruleTitleUnique = Rule::unique((new Category())->getTable(), 'title');

        if ($category = $this->route('category')) {
            $ruleTitleUnique->ignore($category, 'id');
        }
        return [
            'title' => ['required', 'string', $ruleTitleUnique],
        ];
    }
}
