<?php

namespace App\Services;

use App\Models\Product;

/**
 * Product Service
 */
class ProductService
{
    /**
     * Create product using provided data
     *
     * @param $data
     * @return mixed
     */
    public function store($data): mixed
    {
        $product = Product::create($data);

        $product->categories()->sync(array_values($data['categories']));

        return $product->load('categories');
    }

    /**
     * Update provided product using data array
     *
     * @param Product $product
     * @param $data
     * @return mixed
     */
    public function update(Product $product, $data): mixed
    {
        $product->update($data);

        $product->categories()->sync(array_values($data['categories']));

        return $product->load('categories');
    }
}
